import torch
import torch.nn as nn
import torch.nn.functional as F
from modules.layer import ConvBlock, HighwayConvBlock, ConvBlockGenerated, HighwayConvBlockGenerated


class TextEncoder(nn.Module):
    def __init__(self, channels, kernel_size, depth, n_symbols):
        super().__init__()
        padding = (kernel_size - 1) // 2
        self.cnn = list()
        for _ in range(depth):
            self.cnn.append(nn.Sequential(
                nn.Conv1d(channels, channels, kernel_size=kernel_size, padding=padding),
                nn.ReLU(),
                nn.Dropout(0.5),
            ))
        self.cnn = nn.Sequential(*self.cnn)

        self.lstm = nn.LSTM(channels, channels//2, 1, batch_first=True, bidirectional=True)

    def forward(self, x, input_lengths):
        #x = self.embedding(x)  # [B, T, emb]
        x = x.transpose(1, 2)  # [B, emb, T]
        x = self.cnn(x)  # [B, chn, T]
        x = x.transpose(1, 2)  # [B, T, chn]

        input_lengths = input_lengths.cpu().numpy()
        x = nn.utils.rnn.pack_padded_sequence(
            x, input_lengths, batch_first=True)

        self.lstm.flatten_parameters()
        x, _ = self.lstm(x)
        x, _ = nn.utils.rnn.pad_packed_sequence(
            x, batch_first=True)

        return x

    def inference(self, x):
        x = x.transpose(1, 2)
        x = self.cnn(x)
        x = x.transpose(1, 2)
        self.lstm.flatten_parameters()
        x, _ = self.lstm(x)
        return x


class ConditionalEncoder(torch.nn.Module):
    """Encoder with language embeddings concatenated to each input character embedding.
    Arguments:
        input_dim -- total number of languages in the dataset
        langs_embedding_dim -- output size of the language embedding
        encoder_args -- tuple or list of arguments for encoder (input dimension without the embedding dimension), see Encoder class
    """

    def __init__(self, num_langs, langs_embedding_dim, encoder_args):
        super(ConditionalEncoder, self).__init__()
        # modify input_dim of the underlying Encoder
        encoder_args = list(encoder_args)
        self.embedding = nn.Embedding(encoder_args[3], encoder_args[0])

        encoder_args[0] += langs_embedding_dim
        encoder_args = tuple(encoder_args)
        self._encoder = TextEncoder(*encoder_args)

    def forward(self, x, x_lengths, l):
        x = self.embedding(x)
        x = torch.cat((x, l), dim=-1)
        x = self._encoder(x, x_lengths)
        return x

    def inference(self, x, l):
        x = self.embedding(x)
        x = torch.cat((x, l), dim=-1)
        x = self._encoder.inference(x)
        return x