import torch
from torch.nn import functional as F
from torch.nn import Sequential, Linear, ReLU, Sigmoid, Tanh, Identity, Dropout, Conv1d, ConstantPad1d, BatchNorm1d, \
    LSTMCell

def get_activation(name):
    """Get activation function by name."""
    return {
        'relu': ReLU(),
        'sigmoid': Sigmoid(),
        'tanh': Tanh(),
        'identity': Identity()
    }[name]


class ZoneoutLSTMCell(torch.nn.Module):
    def __init__(self, input_size, hidden_size, bias=True, zoneout_prob=0.1):
        super().__init__()
        self.input_size = input_size
        self.hidden_size = hidden_size
        self.zoneout_prob = zoneout_prob
        self.lstm = LSTMCell(input_size, hidden_size, bias)
        self.dropout = Dropout(p=zoneout_prob)

        # initialize all forget gate bias of LSTM to 1.0
        # https://discuss.pytorch.org/t/set-forget-gate-bias-of-lstm/1745/4
        self.lstm.bias_ih[hidden_size:2*hidden_size].data.fill_(1.0)
        self.lstm.bias_hh[hidden_size:2*hidden_size].data.fill_(1.0)

    def forward(self, x, prev_hc=None):
        h, c = self.lstm(x, prev_hc)

        if prev_hc is None:
            prev_h = torch.zeros(x.size(0), self.hidden_size)
            prev_c = torch.zeros(x.size(0), self.hidden_size)
        else:
            prev_h, prev_c = prev_hc

        if self.training:
            h = (1. - self.zoneout_prob) * self.dropout(h - prev_h) + prev_h
            c = (1. - self.zoneout_prob) * self.dropout(c - prev_c) + prev_c
        else:
            h = (1. - self.zoneout_prob) * h + self.zoneout_prob * prev_h
            c = (1. - self.zoneout_prob) * c + self.zoneout_prob * prev_c

        return h, c


class ConvBlock(torch.nn.Module):
    """
    One dimensional convolution with batchnorm and dropout, expected channel-first input.

    Arguments:
        input_channels -- number if input channels
        output_channels -- number of output channels
        kernel -- convolution kernel size ('same' padding is used)
    Keyword arguments:
        dropout (default: 0.0) -- dropout rate to be aplied after the block
        activation (default 'identity') -- name of the activation function applied after batchnorm
        dilation (default: 1) -- dilation of the inner convolution
        groups (default: 1) -- number of groups of the inner convolution
        batch_norm (default: True) -- set False to disable batch normalization
    """

    def __init__(self, input_channels, output_channels, kernel,
                 dropout=0.0, activation='identity', dilation=1, groups=1, batch_norm=True):
        super(ConvBlock, self).__init__()

        self._groups = groups

        p = (kernel - 1) * dilation // 2
        padding = p if kernel % 2 != 0 else (p, p + 1)
        layers = [ConstantPad1d(padding, 0.0),
                  Conv1d(input_channels, output_channels, kernel, padding=0, dilation=dilation, groups=groups,
                         bias=(not batch_norm))]

        if batch_norm:
            layers += [BatchNorm1d(output_channels)]

        layers += [get_activation(activation)]
        layers += [Dropout(dropout)]

        self._block = Sequential(*layers)

    def forward(self, x):
        return self._block(x)


class ConvBlockGenerated(torch.nn.Module):
    """One dimensional convolution with generated weights and with batchnorm and dropout, expected channel-first input.

    Arguments:
        embedding_dim -- size of the meta embedding
        bottleneck_dim -- size of the generating layer
        input_channels -- number if input channels
        output_channels -- number of output channels
        kernel -- convolution kernel size ('same' padding is used)
    Keyword arguments:
        dropout (default: 0.0) -- dropout rate to be aplied after the block
        activation (default 'identity') -- name of the activation function applied after batchnorm
        dilation (default: 1) -- dilation of the inner convolution
        groups (default: 1) -- number of groups of the inner convolution
        batch_norm (default: True) -- set False to disable batch normalization
    """

    def __init__(self, embedding_dim, bottleneck_dim, input_channels, output_channels, kernel,
                 dropout=0.0, activation='identity', dilation=1, groups=1, batch_norm=True):
        super(ConvBlockGenerated, self).__init__()

        self._groups = groups

        p = (kernel - 1) * dilation // 2
        padding = p if kernel % 2 != 0 else (p, p + 1)

        self._padding = ConstantPad1d(padding, 0.0)
        self._convolution = Conv1dGenerated(embedding_dim, bottleneck_dim, input_channels, output_channels, kernel,
                                            padding=0, dilation=dilation, groups=groups, bias=(not batch_norm))
        self._regularizer = BatchNorm1dGenerated(embedding_dim, bottleneck_dim, output_channels,
                                                 groups=groups) if batch_norm else None
        self._activation = Sequential(
            get_activation(activation),
            Dropout(dropout)
        )

    def forward(self, x):
        e, x = x
        x = self._padding(x)
        x = self._convolution(e, x)
        if self._regularizer is not None:
            x = self._regularizer(e, x)
        x = self._activation(x)
        return e, x


class HighwayConvBlock(ConvBlock):
    """Gated 1D covolution.

    Arguments:
        see ConvBlock
    """

    def __init__(self, input_channels, output_channels, kernel,
                 dropout=0.0, activation='identity', dilation=1, groups=1, batch_norm=True):
        super(HighwayConvBlock, self).__init__(input_channels, 2 * output_channels, kernel, dropout, activation,
                                               dilation, groups, batch_norm)
        self._gate = Sigmoid()

    def forward(self, x):
        h = super(HighwayConvBlock, self).forward(x)
        chunks = torch.chunk(h, 2 * self._groups, 1)
        h1 = torch.cat(chunks[0::2], 1)
        h2 = torch.cat(chunks[1::2], 1)
        p = self._gate(h1)
        return h2 * p + x * (1.0 - p)


class HighwayConvBlockGenerated(ConvBlockGenerated):
    """Gated 1D covolution with generated weights.

    Arguments:
        embedding_dim -- size of the meta embedding
        bottleneck_dim -- size of the generating layer
        see ConvBlockGenerated
    """

    def __init__(self, embedding_dim, bottleneck_dim, input_channels, output_channels, kernel,
                 dropout=0.0, activation='identity', dilation=1, groups=1, batch_norm=True):
        super(HighwayConvBlockGenerated, self).__init__(embedding_dim, bottleneck_dim, input_channels,
                                                        2 * output_channels, kernel,
                                                        dropout, activation, dilation, groups, batch_norm)
        self._gate = Sigmoid()

    def forward(self, x):
        e, x = x
        _, h = super(HighwayConvBlockGenerated, self).forward((e, x))
        chunks = torch.chunk(h, 2 * self._groups, 1)
        h1 = torch.cat(chunks[0::2], 1)
        h2 = torch.cat(chunks[1::2], 1)
        p = self._gate(h1)
        return e, h2 * p + x * (1.0 - p)