import math
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from scipy.special import gamma


class LinearNorm(nn.Module):
    def __init__(self, in_dim, out_dim, bias=True, w_init_gain='linear'):
        super(LinearNorm, self).__init__()
        self.linear_layer = torch.nn.Linear(in_dim, out_dim, bias=bias)

        torch.nn.init.xavier_uniform_(
            self.linear_layer.weight,
            gain=torch.nn.init.calculate_gain(w_init_gain))

    def forward(self, x):
        return self.linear_layer(x)


class StaticFilter(nn.Module):
    def __init__(self, channels, kernel_size, out_dim):
        super().__init__()
        assert kernel_size % 2 == 1, \
            'kernel size of StaticFilter must be odd, got %d' % kernel_size
        padding = (kernel_size - 1) // 2

        self.conv = nn.Conv1d(2, channels, kernel_size=kernel_size, padding=padding)
        self.fc = LinearNorm(channels, out_dim, bias=False, w_init_gain='tanh')

    def forward(self, x):
        # prev_attn: [B, T]
        #x = prev_attn.unsqueeze(1)  # [B, 1, T]
        x = self.conv(x)  # [B, channels, T]
        x = x.transpose(1, 2)  # [B, T, out_dim]
        x = self.fc(x)
        return x


class Attention(nn.Module):
    def __init__(self, attn_rnn_dim, attn_dim, static_channels, static_kernel_size, embedding_dim): #이 부분이 바뀜.
        super().__init__()
        self.v = nn.Linear(attn_dim, 1, bias=False)
        self.static_filter = StaticFilter(static_channels, static_kernel_size, attn_dim)
        self.query_layer = LinearNorm(attn_rnn_dim, attn_dim,
                                      bias=False, w_init_gain='tanh')
        self.memory_layer = LinearNorm(embedding_dim, attn_dim, bias=False,
                                       w_init_gain='tanh')
        self.score_mask_value = -float('inf')

    def get_alignment_energies(self, query, processed_memory, attention_weights_cat):
        processed_query = self.query_layer(query.unsqueeze(1))
        processed_attention_weights = self.static_filter(attention_weights_cat)
        energies = self.v(torch.tanh(
            processed_query + processed_attention_weights + processed_memory))

        energies = energies.squeeze(-1)
        return energies

    def forward(self, attn_hidden, memory, processed_memory, attention_weights_cat, mask):
        alignment = self.get_alignment_energies(
            attn_hidden, processed_memory, attention_weights_cat)

        if mask is not None:
            alignment.data.masked_fill_(mask, self.score_mask_value)

        attn_weights = F.softmax(alignment, dim=1)  # [B, T]
        context = torch.bmm(attn_weights.unsqueeze(1), memory)
        # [B, 1, T] @ [B, T, (chn.encoder + chn.speaker)] -> [B, 1, (chn.encoder + chn.speaker)]
        context = context.squeeze(1)

        return context, attn_weights
