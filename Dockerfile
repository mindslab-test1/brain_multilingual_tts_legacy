FROM nvcr.io/nvidia/pytorch:20.09-py3
RUN python3 -m pip --no-cache-dir install --upgrade \
        tensorboard==2.0.0 \
        omegaconf==2.0.0 \
        pytorch_lightning==1.0.3 \
        gpustat==0.6.0 \
        grpcio==1.13.0 \
        grpcio-tools==1.13.0 \
        protobuf==3.6.0 \
        xpinyin==0.5.7 \
        && \
python3 -m pip uninstall -y tensorboard-plugin-dlprof && \
apt update && \
apt install -y \
    tmux \
    htop \
    ncdu && \
apt clean && \
apt autoremove && \
rm -rf /var/lib/apt/lists/* /tmp/* && \
mkdir /root/tacotron
COPY . /root/tacotron
RUN cd /root/tacotron/ && \
    python3 -m grpc.tools.protoc --proto_path=brain_idl/protos/audio/legacy --python_out=. --grpc_python_out=. tts.proto
