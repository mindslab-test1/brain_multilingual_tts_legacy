import os
import tqdm
import torch
from torch.utils.data import DataLoader
import shutil
import argparse
import pytorch_lightning as pl
from omegaconf import OmegaConf

from tacotron import Tacotron
from datasets.text import Language
from datasets import TextMelDataset, text_mel_collate


META_DIR = 'gta_mels'

class GtaExtractor(object):
    def __init__(self, args):
        self.args = args
        self._load_checkpoint(args.checkpoint_path, args.config)
        self.trainloader = self._gen_dataloader(self.language_list, mode='train')
        self.valloader = self._gen_dataloader(self.language_list, mode='val')

    def _gen_hparams(self, config_paths):
        # generate hparams object for pl.LightningModule
        parser = argparse.ArgumentParser()
        parser.add_argument('--config')
        args = parser.parse_args(['--config', config_paths])
        return args

    def _load_checkpoint(self, checkpoint_path, model_config_path):
        args_temp = self._gen_hparams(model_config_path)
        self.model = Tacotron(args_temp).cuda()
        self.hp = self.model.hp
        self.language_list = self.model.language_list

        self.speaker_list = []
        for x in self.language_list:
            self.speaker_list+=x.speakers

        checkpoint = torch.load(checkpoint_path, map_location='cpu')
        self.model.load_state_dict(checkpoint['state_dict'])
        self.model.eval()
        self.model.freeze()
        del checkpoint
        torch.cuda.empty_cache()

    def _gen_dataloader(self, language_list, mode):
        x_list = []
        dir_list = []
        meta_list = []
        if mode == 'train':
            for x in language_list:
                x_list.append(x)
                dir_list.append(x.train_dir)
                meta_list.append(x.train_meta)
        else:
            for x in language_list:
                x_list.append(x)
                dir_list.append(x.val_dir)
                meta_list.append(x.val_meta)

        dataset = TextMelDataset(self.hp, x_list, dir_list, meta_list, train=False)
        return DataLoader(dataset, batch_size=self.hp.train.batch_size, shuffle=False,
                          num_workers=self.hp.train.num_workers,
                          collate_fn=text_mel_collate(1), pin_memory=False, drop_last=False)

    def main(self):
        self.extract_and_write_meta('val')
        self.extract_and_write_meta('train')

    def extract_and_write_meta(self, mode):
        assert mode in ['train', 'val']

        dataloader = self.trainloader if mode == 'train' else self.valloader
        desc = 'Extracting GTA mel of %s data' % mode
        meta_list = list()
        for batch in tqdm.tqdm(dataloader, desc=desc):
            temp_meta = self.extract_gta_mels(batch, mode)
            meta_list.extend(temp_meta)

        root_dir = self.language_list[0].train_dir if mode == 'train' else self.language_list[0].val_dir
        meta_path = self.language_list[0].train_meta if mode == 'train' else self.language_list[0].val_meta
        meta_filename = os.path.basename(meta_path)
        new_meta_filename = 'gta_' + meta_filename
        new_meta_path = os.path.join(root_dir, META_DIR, new_meta_filename)

        os.makedirs(os.path.join(root_dir, META_DIR), exist_ok=True)
        with open(new_meta_path, 'w', encoding='utf-8') as f:
            for wavpath, speaker in meta_list:
                f.write('%s||%s\n' % (wavpath, speaker))

        print('Wrote %d of %d files to %s' % \
            (len(meta_list), len(dataloader.dataset), new_meta_path))

    @torch.no_grad()
    def extract_gta_mels(self, batch, mode):
        text, mel_target, gate_target, speakers, languages, input_lengths, output_lengths, max_input_len, savepaths = batch
        text = text.cuda()
        mel_target = mel_target.cuda()
        speakers = speakers.cuda()
        languages = languages.cuda()
        input_lengths = input_lengths.cuda()
        output_lengths = output_lengths.cuda()
        max_input_len = max_input_len.cuda()

        mel_pred, mel_postnet, gate_outputs, alignment, spk_predict = \
            self.model.forward(
                text, mel_target, speakers, languages, input_lengths, output_lengths, max_input_len,
                prenet_dropout=0.5, tfrate=1.0)
        return self.store_mels_in_savepaths(
            mel_postnet, alignment, input_lengths, output_lengths, savepaths, speakers, languages, mode)

    def min_max_attention(self, attention):
        # attention: [T_dec, T_enc]
        max_values = torch.max(attention, dim=1)[0]
        mma = torch.min(max_values)
        return mma

    def store_mels_in_savepaths(self,
        mel_postnet, alignment, input_lengths, output_lengths, savepaths, speakers, languages, mode):
        mels = mel_postnet.detach().cpu()
        alignment = alignment.detach().cpu()
        input_lengths = input_lengths.cpu()
        output_lengths = output_lengths.cpu()
        speakers = speakers.cpu().tolist()
        languages = languages.cpu().tolist()

        temp_meta = list()
        for i, path in enumerate(savepaths):
            attention = alignment[i]
            t_enc = input_lengths[i]
            t_dec = output_lengths[i]
            speaker_id = speakers[i]
            language_id = languages[i]

            hp_data = self.language_list[language_id]
            speaker = self.speaker_list[speaker_id]
            attention = attention[:t_dec, :t_enc]

            mel = mels[i][:, :t_dec].clone()

            torch.save(mel, path)
            if mel.size(1) < self.args.min_mel_length:
                continue

            # so now, mel is sufficiently long, and alignment looks good.
            # let's write the mel path to metadata.
            root_dir = hp_data.train_dir if mode == 'train' \
                else hp_data.val_dir
            rel_path = os.path.relpath(path, start=root_dir)
            wav_path = rel_path.replace('.predict', '')
            temp_meta.append((wav_path, speaker))

            attn_path = path.replace('.predict', '.attn')
            torch.save(attention, attn_path)

        return temp_meta


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--config', type=str, required=True,
                        help="path of configuration yaml file")    
    parser.add_argument('-g', '--gpus', type=str, default=None,
                        help="gpu device number")
    parser.add_argument('-p', '--checkpoint_path', type=str, default=None,
                        help="path of checkpoint to use for extracting GTA mel")
    parser.add_argument('-m', '--min_mel_length', type=int, default=33,
                        help="minimal length of mel spectrogram. (segment_length // hop_length + 1) expected.")
    parser.add_argument('--mma_threshold', type=float, default=0.35,
                        help="minimal max attention value threshold to check alignment status")
    args = parser.parse_args()

    extractor = GtaExtractor(args)
    extractor.main()
