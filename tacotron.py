import os
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import DataLoader, ConcatDataset
import pytorch_lightning as pl
import random
import numpy as np
from omegaconf import OmegaConf

from datasets.text import Language, all_symbols, _jap_romaji_characters
from modules import TextEncoder, TacotronDecoder, ConditionalEncoder, VAE
from modules.classifier import ReversalClassifier
from datasets import TextMelDataset, text_mel_collate
from utils.alignment_loss import GuidedAttentionLoss


class Tacotron(pl.LightningModule):
    def __init__(self, hparams):
        super().__init__()
        self.save_hyperparameters()  # used for pl
        hp = OmegaConf.load(hparams.config)
        self.hp = hp
        self.language_list = [self.hp.kor, self.hp.eng, self.hp.jap]
       
        self.symbols = all_symbols + _jap_romaji_characters

        self.symbols = ['"{}"'.format(symbol) for symbol in self.symbols]

        self.encoder = ConditionalEncoder(len(self.language_list), hp.chn.language,
                                          (hp.chn.encoder, hp.ker.encoder, hp.depth.encoder, len(self.symbols)))
        self.n_frames_per_step = hp.train.n_frames_per_step
        if hp.train.vae:
            self.vae = VAE(hp.audio.n_mel_channels, hp.chn.latent)
        else:
            self.vae = None

        spk_soo = 0
        for x in self.language_list:
            spk_soo += len(x.speakers)
        self.speaker_embedding = nn.Embedding(spk_soo, hp.chn.speaker)
        self.language_embedding = nn.Embedding(len(self.language_list), hp.chn.language)
        
        self.teacher = TacotronDecoder(hp)
        self.classifier = ReversalClassifier(hp, hp.chn.encoder + hp.chn.language, spk_soo)

        self.is_val_first = True
        self.attn_loss = GuidedAttentionLoss(20000, 0.25, 1.00025)

        self.collate_fn = text_mel_collate(hp.train.n_frames_per_step)

    def forward(self, text, mel_target, speakers, languages, input_lengths, output_lengths, max_input_len,
                prenet_dropout=0.5, no_mask=False, tfrate=0.0):
        language_emb = self.language_embedding(languages)
        language_emb = language_emb.unsqueeze(1).expand(-1, text.size(1), -1)
        text_encoding = self.encoder(text, input_lengths, language_emb)  # [B, T, chn.encoder]

        spk_predict = self.classifier(text_encoding)

        speaker_emb = self.speaker_embedding(speakers)  # [B, chn.speaker]
        speaker_emb = speaker_emb.unsqueeze(1).expand(-1, text_encoding.size(1), -1)  # [B, T, chn.speaker]

        if self.vae is not None:
            _, _, latent_z = self.vae(mel_target, output_lengths)
            latent_z = torch.nn.utils.rnn.pad_sequence(latent_z, batch_first=True)
            latent_z = latent_z.unsqueeze(1).expand(-1, text_encoding.size(1), -1)
            decoder_input = torch.cat((text_encoding, speaker_emb, latent_z), dim=2)  # [B, T, (chn.encoder + chn.speaker)]
        else:
            decoder_input = torch.cat((text_encoding, speaker_emb), dim=2)
        
        mel_pred, mel_postnet, gate_outputs, alignment = \
            self.teacher(mel_target, decoder_input, input_lengths, output_lengths, max_input_len,
                         prenet_dropout, no_mask, tfrate)
        return mel_pred, mel_postnet, gate_outputs, alignment, spk_predict

    def inference(self, text, speakers, languages, prenet_dropout=0.5):
        print(text)
        language_emb = self.language_embedding(languages)
        language_emb = language_emb.unsqueeze(1).expand(-1, text.size(1), -1)

        text_encoding = self.encoder.inference(text, language_emb)
        speaker_emb = self.speaker_embedding(speakers)  # [B, chn.speaker]
        speaker_emb = speaker_emb.unsqueeze(1).expand(-1, text_encoding.size(1), -1)
        if self.vae is not None:
            latent_z = torch.zeros(1,text_encoding.size(1),self.hp.chn.latent)
            device = text_encoding.device
            latent_z = latent_z.to(device)
            decoder_input = torch.cat((text_encoding, speaker_emb, latent_z), dim=2)
        else:
            decoder_input = torch.cat((text_encoding, speaker_emb), dim=2)
        _, mel_postnet, gate_outputs, alignment = \
            self.teacher.inference(decoder_input, prenet_dropout)
        return mel_postnet, gate_outputs, alignment

    def training_step(self, batch, batch_idx):
        text, mel_target, gate_target, speakers, languages, input_lengths, output_lengths, max_input_len, _ = batch
        mel_pred, mel_postnet, gate_outputs, alignment, spk_predict = \
            self.forward(text, mel_target, speakers, languages, input_lengths, output_lengths, max_input_len,\
                         tfrate=self.hp.train.teacher_force.rate)

        classification_loss = self.classifier.loss(input_lengths, speakers, spk_predict)
        loss = F.mse_loss(mel_pred, mel_target) + F.mse_loss(mel_postnet, mel_target) + \
               nn.BCEWithLogitsLoss()(gate_outputs.view(-1, 1), gate_target.view(-1, 1))
        attention_loss = self.attn_loss(alignment, input_lengths, output_lengths, self.global_step)

        self.logger.log_loss(loss, mode='train', step=self.global_step)
        self.logger.log_loss(classification_loss, mode='train.classification', step=self.global_step)
        self.logger.log_loss(attention_loss, mode='train.attention', step=self.global_step)
        
        return {'loss': loss + classification_loss * self.hp.train.lamb + attention_loss}

    def validation_step(self, batch, batch_idx):
        text, mel_target, gate_target, speakers, languages, input_lengths, output_lengths, max_input_len, _ = batch
        mel_pred, mel_postnet, gate_outputs, alignment, spk_predict = \
            self.forward(text, mel_target, speakers, languages, input_lengths, output_lengths, max_input_len,
                         prenet_dropout=0.5, tfrate=1.0)
        loss = F.mse_loss(mel_pred, mel_target) + F.mse_loss(mel_postnet, mel_target) + \
               nn.BCEWithLogitsLoss()(gate_outputs.view(-1, 1), gate_target.view(-1, 1))

        if self.is_val_first: # plot alignment, character embedding
            self.is_val_first = False
            self.logger.log_figures(mel_pred, mel_postnet, mel_target, alignment, gate_target, gate_outputs, self.global_step)
            self.logger.log_embedding(self.symbols, self.encoder.embedding.weight, self.global_step)

        return {'loss': loss}

    def validation_epoch_end(self, outputs):
        loss = torch.stack([x['loss'] for x in outputs]).mean()
        self.logger.log_loss(loss, mode='val', step=self.global_step)
        self.is_val_first = True
        self.log('val_loss', loss)

    def configure_optimizers(self):
        if self.hp.train.fine_tuning:
            learnable_params = list(self.speaker_embedding.parameters()) + list(self.teacher.parameters())
        else:
            learnable_params = self.parameters()
        return torch.optim.Adam(
            learnable_params,
            lr=self.hp.train.adam.lr,
            weight_decay=self.hp.train.adam.weight_decay,
        )

    def lr_lambda(self, step):
        progress = (step - self.hp.train.decay.start) / (self.hp.train.decay.end - self.hp.train.decay.start)
        return self.hp.train.decay.rate ** np.clip(progress, 0.0, 1.0)

    def optimizer_step(self, epoch, batch_idx, optimizer, optimizer_idx, optimizer_closure=None, on_tpu=False, using_native_amp=False, using_lbfgs=False):
        lr_scale = self.lr_lambda(self.global_step)
        for pg in optimizer.param_groups:
            pg['lr'] = lr_scale * self.hp.train.adam.lr

        optimizer.step()
        optimizer.zero_grad()

        self.logger.log_learning_rate(lr_scale * self.hp.train.adam.lr, self.global_step)

    def train_dataloader(self):
        x_list = []
        train_dir_list = []
        train_meta_list = []
        for x in self.language_list:
            x_list.append(x)
            train_dir_list.append(x.train_dir)
            train_meta_list.append(x.train_meta)
        trainset = TextMelDataset(self.hp, x_list, train_dir_list, train_meta_list, True)
        return DataLoader(trainset, batch_size=self.hp.train.batch_size, shuffle=True,
                        num_workers=self.hp.train.num_workers,
                        collate_fn=self.collate_fn, pin_memory=True, drop_last=True)

    def val_dataloader(self):
        x_list = []
        val_dir_list = []
        val_meta_list = []
        for x in self.language_list:
            x_list.append(x)
            val_dir_list.append(x.val_dir)
            val_meta_list.append(x.val_meta)
        valset = TextMelDataset(self.hp, x_list, val_dir_list, val_meta_list, False)

        return DataLoader(valset, batch_size=self.hp.train.batch_size, shuffle=False,
                        num_workers=self.hp.train.num_workers,
                        collate_fn=self.collate_fn, pin_memory=False, drop_last=False)

