import os
import re
import torch
import random
import librosa
import numpy as np
from torch.utils.data import Dataset
from collections import Counter

from .text import Language
from .text.cmudict import CMUDict
from modules.mel import mel_spectrogram

class TextMelDataset(Dataset):
    def __init__(self, hp, data_list, data_dir_list, metadata_path_list, train=True, norm=True):
        super().__init__()
        self.hp = hp
        self.train = train
        self.data = []
        self.lang = []
        self.data_dir = []
        self.meta = []
        self.speaker_dict = {}
        self.cmudict = []
        self.speakers_per_lang = []
        self.norm = norm

        all_speakers = []

        for i,(data, data_dir, metadata_path) in enumerate(zip(data_list, data_dir_list, metadata_path_list)):
            self.data.append(data)
            self.lang.append(Language(data.lang, data.text_cleaners))
            self.data_dir.append(data_dir)
            metadata_path = os.path.join(data_dir, metadata_path)

            all_speakers += data.speakers

            new_meta = self.load_metadata(metadata_path, i)

            self.speakers_per_lang.append(len(data.speakers))

            self.meta += new_meta

            if data.lang == 'eng2':
                self.cmudict.append(CMUDict(data.cmudict_path))
            else:
                self.cmudict.append(None)

        for idx, speaker in enumerate(all_speakers):
            self.speaker_dict[speaker] = idx

        self.speakers_soo = len(all_speakers)

        if train:
            speaker_counter = Counter((spk_id \
                                       for audiopath, text, spk_id, lang_idx in self.meta))
            weights = [self.speakers_soo / self.speakers_per_lang[lang_idx] / speaker_counter[spk_id] \
                       for audiopath, text, spk_id, lang_idx in self.meta]
            self.mapping_weights = torch.DoubleTensor(weights)

        self.cmu_pattern = re.compile(r'^(?P<word>[^!\'(),-.:~?]+)(?P<punc>[!\'(),-.:~?]+)$')

    def __len__(self):
        return len(self.meta)

    def __getitem__(self, idx):
        if self.train:
            idx = torch.multinomial(self.mapping_weights, 1).item()

        audiopath, text, spk_id, lang_idx = self.meta[idx]
        spk_id = self.speaker_dict[spk_id]

        audiopath = os.path.join(self.data_dir[lang_idx], audiopath)
        mel = self.get_mel(audiopath, lang_idx)
        text_norm = self.get_text(text,lang_idx)
        new_path = '{}.predict'.format(audiopath)

        return text_norm, mel, spk_id, lang_idx, new_path

    def get_mel(self, audiopath, lang_idx):

        wav, sr = librosa.load(audiopath, sr=None, mono=True)
        assert sr == self.hp.audio.sampling_rate, \
            'sample mismatch: expected %d, got %d at %s' % (self.hp.audio.sampling_rate, sr, audiopath)
        wav = torch.from_numpy(wav)
        wav = wav.unsqueeze(0)
        if self.norm:
            wav = wav * (0.99 / (torch.max(torch.abs(wav)) + 1e-7))
        mel = mel_spectrogram(wav, self.hp.audio.filter_length, self.hp.audio.n_mel_channels,
                              self.hp.audio.sampling_rate,
                              self.hp.audio.hop_length,
                              self.hp.audio.win_length,
                              self.hp.audio.mel_fmin,
                              self.hp.audio.mel_fmax, center=False)
        mel = mel.squeeze(0)

        return mel

    def get_text(self, text, lang_idx):
        if self.cmudict[lang_idx] is not None and random.random() < 0.5:
            text = ' '.join([self.get_arpabet(word, lang_idx) for word in text.split(' ')])
        text_norm = torch.IntTensor(self.lang[lang_idx].text_to_sequence(text, self.data[lang_idx].text_cleaners))
        return text_norm

    def get_arpabet(self, word, lang_idx):
        arpabet = self.cmudict[lang_idx].lookup(word)
        if arpabet is None:
            match = self.cmu_pattern.search(word)
            if match is None:
                return word
            subword = match.group('word')
            arpabet = self.cmudict[lang_idx].lookup(subword)
            if arpabet is None:
                return word
            punc = match.group('punc')
            arpabet = '{%s}%s' % (arpabet[0], punc)
        else:
            arpabet = '{%s}' % arpabet[0]

        if random.random() < 0.5:
            return word
        else:
            return arpabet

    def load_metadata(self, path, lang_idx, split="|"):
        with open(path, 'r', encoding='utf-8') as f:
            metadata = []
            for line in f:
                t = line.strip().split(split)
                path, text, speaker = t

                metadata.append((path,text,speaker,lang_idx))

        return metadata

class text_mel_collate():

    def __init__(self, n_frames_per_step):
        self.n_frames_per_step = n_frames_per_step

    def __call__(self, batch):
        input_lengths, ids_sorted_decreasing = torch.sort(
            torch.LongTensor([len(x[0]) for x in batch]),
            dim=0, descending=True)
        max_input_len = torch.empty(len(batch), dtype=torch.long)
        max_input_len.fill_(input_lengths[0])

        text_padded = torch.zeros((len(batch), max_input_len[0]), dtype=torch.long)
        n_mel_channels = batch[0][1].size(0)
        max_target_len = max([x[1].size(1) for x in batch])

        mel_padded = torch.zeros(len(batch), n_mel_channels, max_target_len)
        output_lengths = torch.empty(len(batch), dtype=torch.long)
        speakers = torch.empty(len(batch), dtype=torch.long)
        languages = torch.empty(len(batch), dtype=torch.long)
        gate_padded = torch.zeros(len(batch), max_target_len)
        new_paths = []

        for idx, key in enumerate(ids_sorted_decreasing):
            text = batch[key][0]
            text_padded[idx, :text.size(0)] = text
            mel = batch[key][1]
            mel_padded[idx, :, :mel.size(1)] = mel
            gate_padded[idx, mel.size(1) - 1:] = 1
            output_lengths[idx] = mel.size(1)
            speakers[idx] = batch[key][2]
            languages[idx] = batch[key][3]
            new_paths.append(batch[key][4])

        return text_padded, mel_padded, gate_padded, speakers, languages, \
            input_lengths, output_lengths, max_input_len, new_paths

