""" from https://github.com/keithito/tacotron """

'''
Defines the set of symbols used in text input to the model.

The default is a set of ASCII characters that works well for English or text that has been run through Unidecode. For other data, you can modify _characters. See TRAINING_DATA.md for details. '''
from . import cmudict

_pad = '<pad>'
_eos = '</s>'
_sos = '<s>'
_punc = '!\'(),-.:~? '

_eng_characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'

# Prepend "@" to ARPAbet symbols to ensure uniqueness (some are the same as uppercase letters):
_arpabet = ['@' + s for s in cmudict.valid_symbols]

_jamo_leads = "".join([chr(_) for _ in range(0x1100, 0x1113)])
_jamo_vowels = "".join([chr(_) for _ in range(0x1161, 0x1176)])
_jamo_tails = "".join([chr(_) for _ in range(0x11A8, 0x11C3)])
_kor_characters = _jamo_leads + _jamo_vowels + _jamo_tails

_cht_characters = 'abcdefghijklmnopqrstuvwxyz12345'
_esp_characters = 'abcdefghijklmnopqrstuvwxyzñáéíóú'
_fra_characters = 'abcdefghijklmnopqrstuvwxyzçéâêîôûàèùäëïöüœæ'

_all_characters = 'abcdefghijklmnopqrstuvwxyzçèéßäöōǎǐíǒàáǔüèéìūòóùúāēěīâêôûñőűабвгдежзийклмнопрстуфхцчшщъыьэюяё'

_cmu_characters = [
    'AA', 'AE', 'AH',
    'AO', 'AW', 'AY',
    'B', 'CH', 'D', 'DH', 'EH', 'ER', 'EY',
    'F', 'G', 'HH', 'IH', 'IY',
    'JH', 'K', 'L', 'M', 'N', 'NG', 'OW', 'OY',
    'P', 'R', 'S', 'SH', 'T', 'TH', 'UH', 'UW',
    'V', 'W', 'Y', 'Z', 'ZH'
]
'''
_cmu_characters = [
  'AA0', 'AA1', 'AA2', 'AE0', 'AE1', 'AE2', 'AH0', 'AH1', 'AH2',
  'AO0', 'AO1', 'AO2', 'AW0', 'AW1', 'AW2', 'AY0', 'AY1', 'AY2',
  'B', 'CH', 'D', 'DH', 'EH0', 'EH1', 'EH2', 'ER0', 'ER1', 'ER2',
  'EY0', 'EY1', 'EY2', 'F', 'G', 'HH', 'IH0', 'IH1', 'IH2',  'IY0', 'IY1',
  'IY2', 'JH', 'K', 'L', 'M', 'N', 'NG', 'OW0', 'OW1', 'OW2', 'OY0',
  'OY1', 'OY2', 'P', 'R', 'S', 'SH', 'T', 'TH', 'UH0', 'UH1', 'UH2',
  'UW0', 'UW1', 'UW2', 'V', 'W', 'Y', 'Z', 'ZH'
]
'''
_cmu_characters = ['@' + s for s in _cmu_characters]

_jap_romaji_characters = [
     'N', 'a', 'b', 'by', 'ch', 'cl', 'd', 'dy', 'e', 'f', 'g',
     'gy', 'h', 'hy', 'i', 'j', 'k', 'ky', 'm', 'my', 'n', 'ny',
     'o', 'p', 'pau', 'py', 'r', 'ry', 's', 'sh', 't', 'ts', 'u',
     'v', 'w', 'y', 'z'
]

#_jap_romaji_characters = ['by', 'ch', 'cl', 'dy', 'gy', 'hy', 'ky', 'my', 'ny', 'pau', 'py', 'ry', 'sh', 'ts']

_jap_kana_characters = [
     '、',
     'ぁ', 'あ', 'ぃ', 'い', 'ぅ', 'う', 'ぇ', 'え', 'ぉ', 'お',
     'か', 'が', 'き', 'ぎ', 'く', 'ぐ', 'け', 'げ', 'こ', 'ご',
     'さ', 'ざ', 'し', 'じ', 'す', 'ず', 'せ', 'ぜ', 'そ', 'ぞ',
     'た', 'だ', 'ち', 'っ', 'つ', 'づ', 'て', 'で', 'と', 'ど',
     'な', 'に', 'ぬ', 'ね', 'の', 'は', 'ば', 'ぱ', 'ひ', 'び',
     'ぴ', 'ふ', 'ぶ', 'ぷ', 'へ', 'べ', 'ぺ', 'ほ', 'ぼ', 'ぽ',
     'ま', 'み', 'む', 'め', 'も', 'ゃ', 'や', 'ゅ', 'ゆ', 'ょ',
     'よ', 'ら', 'り', 'る', 'れ', 'ろ', 'わ', 'を', 'ん', 'ゔ',
     'ー'
]

# Export all symbols:
eng_symbols = [_pad, _eos] + list(_punc) + [_sos] + _arpabet + list(_eng_characters)
cmu_symbols = [_pad, _eos] + list(_eng_characters) + list(_punc) + _cmu_characters + [_sos]

kor_symbols = [_pad, _eos] + list(_punc) + [_sos] + list(_kor_characters)

cht_symbols = [_pad, _eos] + list(_punc) + [_sos] + list(_cht_characters)

jap_romaji_symbols = [_pad, _eos] + list(_punc) + [_sos] + _jap_romaji_characters
jap_kana_symbols = [_pad, _eos] + list(_punc) + [_sos] + _jap_kana_characters

esp_symbols = [_pad, _eos] + list(_punc) + [_sos] + list(_esp_characters)
fra_symbols = [_pad, _eos] + list(_punc) + [_sos] + list(_fra_characters)
rus_symbols = [_pad, _eos] + list(_punc) + [_sos] + list('абвгдежзийклмнопрстуфхцчшщъыьэюяё')

else_symbols = [_pad, _eos] + list(_punc) + [_sos]

all_symbols = else_symbols + list(_kor_characters) + list(_all_characters) + _cmu_characters # japanese 제외 전체가 들어가 있음